package com.example.tvwebapp;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyWebViewClient extends WebViewClient {

    private String mHost;

    public MyWebViewClient(String webSiteUrl) {
        super();
        mHost = Uri.parse(webSiteUrl).getHost();
    }

    @Override
    /*
     * La fonction shouldOverrideUrlLoading permet de restreindre l'url configurée avec loadurl
     * à une chaîne de caractère précise, soit le nom de domaine dans le script suivant
     */
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if(Uri.parse(url).getHost().endsWith(mHost)) {
            return false;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        view.getContext().startActivity(intent);
        return true;
    }
}
