package com.example.tvwebapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private String TAG = "TV_WEB_APP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get Deeplink Data
        Intent intent = getIntent();
        Uri deeplinkUri = intent.getData();

        // Remove Top Tool Bar of activity:
        getSupportActionBar().hide();

        // Init Web View
        WebView myWebView = findViewById(R.id.webview);

        myWebView.setInitialScale(getScale());

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setDomStorageEnabled(true);

        String webSiteUrl = getWebSiteUrl(deeplinkUri);

        myWebView.setWebViewClient(new MyWebViewClient(webSiteUrl) {
            @Override
            // Fonction qui permet l'affichage de la page lorsque tout est chargé (événement onPageFinished)

            public void onPageFinished(WebView view, String url) {
                findViewById(R.id.webview).setVisibility(View.VISIBLE);
            }
        });

        if(webSiteUrl.indexOf(".itv.") >= 0) {
            // ITV require to override viewport of web view:
            webSettings.setUseWideViewPort(true);
            // webSettings.setLoadWithOverviewMode(true);
        } else {
            // Override user agent with those used on STB
            // (not supported by ITV)
            webSettings.setUserAgentString("Mozilla/4.0 (compatible; MSIE %i; Windows 98; Linux %o) FVP_STB_BCM7260A0/1.0 (; Netgem; N7950; %v; 1.0; com.netgem.7950;) FVC/3.0 (Netgem; com.netgem.7950;)");
        }

        myWebView.loadUrl(getWebSiteUrl(deeplinkUri));
    }

    private String getWebSiteUrl(Uri deeplinkUri) {
        String strUri = (deeplinkUri != null) ? deeplinkUri.toString() : "";
        String url;
        String scheme = (deeplinkUri != null) ? deeplinkUri.getScheme() : "default";
        String assetId = "";
        String baseUrl = "http://172.17.17.36/android/";
        baseUrl = "http://www.example.com";
        // baseUrl = "http://app.10ft.itv.com/freeview/index.html";
        // baseUrl = "https://www.alsacreations.com/xmedia/tuto/html5/video/exemples.html";
        // baseUrl = "https://www.jwplayer.com/developers/stream-tester/";

        Pattern pattern;
        Matcher matcher;

        switch (scheme) {
            case "bbciplayer":
                baseUrl = "https://www.live.bbctvapps.co.uk/tap/iplayer/?deeplink=tv%2Fplayback%2F{assetid}&lloc=recommendations";
                assetId = "p07ptcxc";

                pattern = Pattern.compile("(.*)(/episode/)(.*)");
                matcher = pattern.matcher(strUri);
                if (matcher.matches()) {
                    for (int i=0; i<matcher.groupCount(); i++)
                        Log.d(TAG, matcher.group(i));
                    assetId = matcher.group(3);
                }
                break;

            case "intent":
                if (strUri.indexOf("uktvplay") > 0) {
                    baseUrl = "https://tvappreg.uktv.co.uk/?brand=default&model=fvp&launch_mode=video_only&house_num={assetid}";
                    assetId = "CTBO902Y";

                    pattern = Pattern.compile("(intent://video/)(.*)(#Intent;)(.*)");
                    matcher = pattern.matcher(strUri);
                    if (matcher.matches()) {
                        for (int i=0; i<matcher.groupCount(); i++)
                            Log.d(TAG, matcher.group(i));
                        assetId = matcher.group(2);
                    }
                }
                break;

            case "uktvplay":
                baseUrl = "https://tvappreg.uktv.co.uk/?brand=default&model=fvp&launch_mode=video_only&house_num={assetid}";
                assetId = "CTBO902Y";
                break;

            case "itvplayer":
                baseUrl = "http://app.10ft.itv.com/freeview/index.html?productionId={assetid}&referrer=recommendation";
                assetId = "1_8694_8644.001";

                pattern = Pattern.compile("(.*)(/episode/)((_|.|[0-9])*)");
                matcher = pattern.matcher(strUri);
                if (matcher.matches() && matcher.groupCount() == 4) {
                    for (int i=0; i<matcher.groupCount(); i++)
                        Log.d(TAG, matcher.group(i));
                    assetId =  matcher.group(3);
                }

                break;

            case "all4":
                baseUrl = "https://fvc-p06.channel4.com/fvc-webapp/index.html#/views/brands?brand=manmarziyaan&programme={assetid}";
                assetId = "69015-001";
                break;

            case "my5":
            default:
        }

        url = baseUrl.replace("{assetid}", assetId);

        Log.d(TAG,"getWebSiteUrl: " + deeplinkUri);
        Log.d(TAG,"        > url: " + url);

        return url;
    }

    private int getScale(){
        double contentWidth = 1280.0;
        if (true) {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            Double val = (displaymetrics.widthPixels / contentWidth) * 100d;
            Log.d(TAG, "widthPixels: " + displaymetrics.widthPixels + " Scale Factor: " + val.intValue());
            return val.intValue();
        } else {
            return 150;
        }
    }
}


